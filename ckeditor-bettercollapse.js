/**
 * @file
 * CKEditor Better Collapse.
 */

(function ($, Drupal, CKEDITOR) {
    CKEDITOR.plugins.add('ckeditor_bettercollapse', {

        beforeInit: function beforeInit(editor) {
            /**
             * On instanceReady move 1st row out of collapsible.
             *
             * @see https://github.com/ckeditor/ckeditor-dev/blob/64749bb245d1e91f6a4ac4e97c9648ec47acda91/plugins/toolbar/plugin.js#L185
             */
            editor.on('instanceReady', function (event) {
                var $container = $(event.editor.container.$);
                var $toolbox = $container.find('.cke_toolbox');
                var $toolboxMain = $toolbox.find('.cke_toolbox_main');
                var $toolbarBreaks = $toolboxMain.find('.cke_toolbar_break');

                if ($toolbarBreaks.length === 1) {
                    var $toolbarMain = $toolbox.find('.cke_toolbox_main');
                    $toolbarMain.show();
                    var $collapser = $toolbox.find('.cke_toolbox_collapser');
                    $collapser.hide();
                }
                else if ($toolbarBreaks.length > 1) {
                    var $toolbarBreak1 = $toolbarBreaks.first();
                    var $toolbarBreak2 = $toolbarBreaks.eq(1);
                    var $firstRow = $($toolbarBreak1.prevAll().toArray().reverse());

                    $firstRow.each(function (el) {
                        var $elementOfFirstRow = $($firstRow[el]);
                        $elementOfFirstRow.insertBefore($toolboxMain);
                    });
                    $toolbarBreak2.remove();
                }
            });
        }
    });

})(jQuery, Drupal, CKEDITOR);
